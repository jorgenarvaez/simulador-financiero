<?php
/*
* Plugin Name: Simulador Financiero
* Description: Simulador Financiero by JNM
* Version: 0.0.1
* Author: Jorge Narváez de la Mora
* Author URI: https://narvaezdelamora.com
*/

function scripts_simfinanciero() {
    $plugin_url = plugin_dir_url( __FILE__ );

    wp_enqueue_style( 'simfinanciero-css', $plugin_url . 'css/simfinanciero.css' );
    wp_enqueue_script('simfinanciero-js', $plugin_url . 'js/simfinanciero.js', true );
}
add_action( 'wp_enqueue_scripts', 'scripts_simfinanciero' );



function simfinanciero($atts) {

	// Defaults
	$values = shortcode_atts(array(
		'cuotas' => 3,
		'importes' => 2,
		'comision1' => 1.5,
		'comisione' => 3.5,
		'avales' => 5
	), $atts);

	echo '<div class="simcontainer">';
	echo '<form action="" method="post">';
	echo '<div class="col50">Importe a calcular</div>';
	echo '<div class="col50">
			<input type="number" name="initial">
			<input type="submit" value="Calcular">
		</div>';
	echo '</form>';

	$initial = preg_replace('[^0-9\+-\*\/\(\) ]', '', $_POST['initial']);

	$cuota = $initial / $values['cuotas'];
	$importe = 5;
	$primercomision = $initial * $values['comision1'];
	$estudiocomision = $initial * $values['comisione'];
	$aval = $values['avales'];




	echo '<div class="col50">Cuotas Necesarias</div> <div class="box-resultado">';
	if (!empty($_POST['initial'])) {
		echo $cuota;
	}  else {
		echo '&nbsp;';
	}
	echo '</div>';

	echo '<div class="col50">Importe por cuotas</div> <div class="box-resultado">';
	if (!empty($_POST['initial'])) {
		echo $importe;
	}  else {
		echo '&nbsp;';
	}
	echo '</div>';

	echo '<div class="col50">Comisión primer año ('. $values['comision1'] .'%)</div> <div class="box-resultado">';
	if (!empty($_POST['initial'])) {
		echo $primercomision;
	}  else {
		echo '&nbsp;';
	}
	echo '</div>';

	echo '<div class="col50">Comisión por estudio ('. $values['comisione'] .'%)</div> <div class="box-resultado">';
	if (!empty($_POST['initial'])) {
		echo $estudiocomision;
	}  else {
		echo '&nbsp;';
	}
	echo '</div>';

	echo '<div class="col50">Total coste del aval técnico</div> <div class="box-resultado">';
	if (!empty($_POST['initial'])) {
		echo $aval;
	}  else {
		echo '&nbsp;';
	}
	echo '</div>';


	/*if (!empty($_POST['initial'])) {
		echo '<div class="sim-box">Valor inicial: ' . $initial . '</div>';
		echo '<div class="sim-box">Sumarle: ' .  $values['sumador'] . '</div>';
		echo '<div class="sim-box resultados">Total suma: ' .  $sumado . '</div>';
		echo '<div class="sim-box">Multiplicar por: ' . $values['multiplicador'] . '</div>';
		echo '<div class="sim-box resultados">Total multiplicación: ' . $multiplicado . '</div>';
	}*/
	echo '</div>';

}
add_shortcode('simulador_financiero', 'simfinanciero');

?>